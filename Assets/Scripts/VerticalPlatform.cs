﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VerticalPlatform : MonoBehaviour {

	private PlatformEffector2D effector;
	public float waitTime;
	public bool touchPlayer = false;
	// Use this for initialization
	void Start () {
		effector = GetComponent<PlatformEffector2D>();
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyUp(KeyCode.DownArrow)){
			// Debug.Log("key Up");
			waitTime = 0.01f;
		}

		if(touchPlayer && Input.GetKey(KeyCode.DownArrow)){
			// Debug.Log("key down");
			if(waitTime <= 0){
				effector.rotationalOffset = 0;
				waitTime = 0.01f;
			} else {
				waitTime -= Time.deltaTime;
			}
		} else{
			effector.rotationalOffset = 180;
		}
		// else {
		// 	effector.rotationalOffset = 180;
		// }

		// if(Input.GetKey(KeyCode.UpArrow)){
		// 	effector.rotationalOffset = 180;
		// }
	}

	void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag == "Player1"){
			touchPlayer = true;
			// Debug.Log("touched");
		}
		// if(other.tag == "StairsStopper"){
		// 	playerRigidBody.gravityScale = 0;
		// }
    }

	void OnTriggerExit2D(Collider2D other)
    {
       if(other.tag == "Player1"){
			// Debug.Log("untouched");
			touchPlayer = false;
		}
    }
}
