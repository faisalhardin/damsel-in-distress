﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chest : MonoBehaviour, ITrapable, IInteractable {
	private bool looted = false;
	private bool trapped = true;

	public void Interact(){
		// if(!trapped){
		// 	Debug.Log("Chest");
		// } else {
		// 	Debug.Log("health - 1");
		// 	trapped = false;
		// }

		if(!looted){
			looted = true;
		} 
	}

	public void setTrap(){
		trapped = true;
		Debug.Log("trap set up");
	}
	
	// void OnTriggerStay2D(Collider2D other){
	// 	if(other.tag == "Player1" && Input.GetKeyDown(KeyCode.C)){
	// 		looted = true;
	// 		Interact();
	// 		Debug.Log("click");
	// 	}
	// }
}
