﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FurnitureSpawner : MonoBehaviour {
	private FurnitureTemplates templates;
	private int rand;
	public HashSet<int> furnitureOpenLocation = new HashSet<int>();
	public List<GameObject> roomFurnitures = new List<GameObject>();
	public int furnitureCount;
	private float roomLength = 3f;
	private const int NUMBER_OF_SLOT = 6;
	public int maxNumberOfFurniture = 2;

	// public bool isCellDoorLocation = false;
	// Use this for initialization
	void Start () {
		templates = GameObject.FindGameObjectWithTag("Furniture").GetComponent<FurnitureTemplates>();
		// furnitureOpenLocation = new HashSet<int>();
		bool willSpawnFurniture = Random.Range(0,10) >= 6;
		Debug.Log(willSpawnFurniture);
		if(willSpawnFurniture){

			int numberOfFurniture = Random.Range(1,maxNumberOfFurniture+1);
			while(numberOfFurniture > 0){

				int randomSlot = Random.Range(0,6);
				if(!furnitureOpenLocation.Contains(randomSlot)){
					numberOfFurniture--;
					SpawnFurniture(randomSlot);
				}

			}
		}
		
		furnitureCount = roomFurnitures.Count ;

	}

	public void SpawnFurniture(int slotIndex){
		rand = Random.Range(0,templates.furnitures.Length);
		roomFurnitures.Add(Instantiate(templates.furnitures[rand], transform.position + Vector3.right * slotIndex *(roomLength/NUMBER_OF_SLOT), Quaternion.identity) as GameObject);
		furnitureOpenLocation.Add(slotIndex);
	}

	public void DeleteFurniture(){
		for (int i = 0; i < roomFurnitures.Count; i++)
		{
			Debug.Log("destroyed");
			Destroy(roomFurnitures[i]);
		}
	}

	public void SpawnCellDoor(){
		bool hasSpawn = false;
		templates = GameObject.FindGameObjectWithTag("Furniture").GetComponent<FurnitureTemplates>();
		while(!hasSpawn){

				int randomSlot = Random.Range(0,6);
				if(!furnitureOpenLocation.Contains(randomSlot)){
					hasSpawn = true;
					rand = Random.Range(0,templates.furnitures.Length);
					Instantiate(templates.cellDoor, transform.position + Vector3.right * randomSlot *(roomLength/NUMBER_OF_SLOT), Quaternion.identity);
					furnitureCount++;
					furnitureOpenLocation.Add(randomSlot);
				}

			}
	}
	


}
