﻿using UnityEngine;
using System.Collections;

public class CharacterControllerScript : MonoBehaviour {

	public Transform ballSource;
	public float ballSpeed = 100f;
	public float maxSpeed = 1f;
	private float leap = 80f;
	public bool facingRight = true;

	public float verticalModifier = 0;

	public float startDashTime = 0.1f;
	private bool isDashing = false;
	private float dashCoolDown = 0;
	private const float DASH_COOLDOWN = 1f;

	public float raycastDistance = 1f;
	public LayerMask whatIsLadder;
	private bool isClimbing;

	private float dashTime;

	Animator anim;
	Rigidbody2D playerRigidBody;

	private float[] coolDown = new float[] {1f,0,60f};
	private bool[] isCoolDown = new bool[] {false,false,false};

	private float inputHorizontal;
	private float inputVertical;

	private bool itermRequirement = true;

	//test control scheme
	private ControlAssignment controlScheme;

	void Start () {

		playerRigidBody = GetComponent<Rigidbody2D> ();
		controlScheme = gameObject.GetComponent<ControlAssignment>();
		dashTime = startDashTime;
	}
	
	// Update is called once per frame
	void FixedUpdate(){


		// if(Input.GetButtonDown(controlScheme.attack)){
		// 	Debug.Log("p1 attack");
		// }

		inputHorizontal = Input.GetAxisRaw ("Horizontal");

		Move (inputHorizontal);

		if(Input.GetButtonDown("Jump") && !isDashing && dashCoolDown < 0.1){
			dashCoolDown = DASH_COOLDOWN;
			isDashing = true;
			Debug.Log("isDashing "+ isDashing);
		}


		if(isDashing){
			if(dashTime > 0){

				Action1 ();	
				dashTime -= Time.deltaTime;
				
			} else{
				dashTime = startDashTime;
				isDashing = false;
			}
		}
		
		DashCoolDown();

		RaycastHit2D hitInfo = Physics2D.Raycast(transform.position, Vector2.up, raycastDistance, whatIsLadder);

		if(hitInfo.collider != null){
			if(Input.GetKeyDown(KeyCode.UpArrow)){
				isClimbing = true;
			}
		} else {
			if(Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.RightArrow)){
				isClimbing = false;
			}
		}

		if(isClimbing == true && hitInfo.collider != null){
			inputVertical = Input.GetAxisRaw("Vertical");
			playerRigidBody.velocity = new Vector2(playerRigidBody.velocity.x, inputVertical* 1);
			playerRigidBody.gravityScale = 0;
		} else {
			playerRigidBody.gravityScale = 4;
		}

	}

	private void DashCoolDown()
	{
		if (dashCoolDown > 0) {
			dashCoolDown -= Time.deltaTime;
		} else {
			dashCoolDown = 0;
		}
	}

	void Move (float h)
	{

		playerRigidBody.velocity = new Vector2(h * maxSpeed, playerRigidBody.velocity.y );
		if(!isDashing){
			if (h > 0 && !facingRight) {
				Flip ();
			} else if (h < 0 && facingRight) {
				Flip ();
			}
		}
	}

	void Flip()
	{
		facingRight = !facingRight;
		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;
	}

	/*
	 * Dash ke depan
	 */
	void Action1(){
		// Vector2 force;
		Debug.Log ("dash");
		if (facingRight) {
			playerRigidBody.velocity = Vector2.right * leap;
			Debug.Log ("right");
		} else {
			playerRigidBody.velocity = Vector2.left * leap;
			Debug.Log ("left");
		}

	}

	// void OnTriggerStay2D(Collider2D other)
    // {
	// 	if(other.tag == "CellDoor"){
			
	// 		if(Input.GetKeyDown(KeyCode.C)){
	// 			Debug.Log("you win");
	// 		}
	// 	}
    //     // if(other.tag == "CellDoor" && Input.GetKeyDown(KeyCode.C)){
	// 	// 	Debug.Log("you win");
	// 	// }

	// 	// if(other.tag == "StairsStopper"){
	// 	// 	playerRigidBody.gravityScale = 0;
	// 	// }
    // }

	// void OnTriggerStay2D(Collider2D other){
	// 	if(other.tag == "Stairs"){
	// 		Debug.Log("stairs");
	// 		playerRigidBody.velocity = new Vector2(10, 100);
	// 	}
		
	// }

	// void OnTriggerExit2D(Collider2D other)
    // {
    //    if(other.tag == "Stairs"){
	// 		verticalModifier = 0;
	// 		playerRigidBody.velocity = new Vector2( playerRigidBody.velocity.x, 1);
	// 		playerRigidBody.gravityScale = 4f;
	// 	}
	// 	if(other.tag == "StairsStopper"){
	// 		playerRigidBody.gravityScale = 4f;
	// 	}
    // }
}
