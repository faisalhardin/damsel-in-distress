﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractionScript : MonoBehaviour {

	private ControlAssignment controlScheme;
	// Use this for initialization
	void Start () {
		controlScheme = gameObject.GetComponent<ControlAssignment>();
	}
	
	// Update is called once per frame
	void Update () {
		// if(Input.GetButtonDown(controlScheme.attack)){
		// 	Debug.Log("interact");
		// }

		
	}

	void OnTriggerStay2D(Collider2D other){
		if(other.tag == "Chest"){
			if(Input.GetButtonDown(controlScheme.interact)){
				other.gameObject.GetComponent<IInteractable>().Interact();

				// if(!looted){
				// 	float rand = Random.Range(0.0f, 1.0f);
				// 	if(rand > 0.7f){
				// 		Debug.Log("get stuff");
				// 	} else {
				// 		Debug.Log("empty");
				// 	}
				// }  else {
				// 	Debug.Log("Looted");
				// }
			}
			
		}

		if(other.tag == "Furniture"){
			if(Input.GetButtonDown(controlScheme.interact)){
				other.gameObject.GetComponent<IInteractable>().Interact();

				// if(!looted){
				// 	float rand = Random.Range(0.0f, 1.0f);
				// 	if(rand > 0.5f){
				// 		Debug.Log("get stuff");
				// 	} else {
				// 		Debug.Log("empty");
				// 	}
				// }  else {
				// 	Debug.Log("Looted");
				// }
			}
		}
	}
}
