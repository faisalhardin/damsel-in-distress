﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomRealizer : MonoBehaviour {
	
	private RoomGenerate roomPatternGenerator;
	private int[,] roomPattern;
	private RoomPatternTemplates templates;
	public List<GameObject> spawnableRoom = new List<GameObject>();
	public int[] cellKey;

	// Use this for initialization
	void Start () {
		templates = GameObject.FindGameObjectWithTag("Rooms").GetComponent<RoomPatternTemplates>();
		roomPatternGenerator = new RoomGenerate(23,11);
		roomPattern = roomPatternGenerator.getRoomPattern();
		// cellKey = [0,0,0];
		// spawnableRoom = new List<GameObject>();

		SpawnRoom(roomPattern);

		SpawnDamselCell();
	}
	

	void SpawnRoom(int[,] roomPattern){
		int xDimension = roomPattern.GetLength(0);
		int yDimension = roomPattern.GetLength(1);

		int x = 0;
		int y = 0;
		while(x < xDimension){
			y = 0;
			while(y<yDimension){
				if(roomPattern[x,y] == 0){
					Instantiate(templates.noRoom, new Vector2(y,x), Quaternion.identity);
				} else if(roomPattern[x,y] == 9){
					Instantiate(templates.verticalRoom, new Vector2(y,x), Quaternion.identity);
				} else if(roomPattern[x,y] == 3){
					spawnableRoom.Add(Instantiate(templates.size3Room, new Vector2(y+1,x), Quaternion.identity) as GameObject);
					// Instantiate(templates.size3Room, new Vector2(y+1,x), Quaternion.identity);
					y += 2;
				}
				y++;
			}
			x++;
		}

		for(int i = 0; i<xDimension; i++){
			Instantiate(templates.noRoom, new Vector2(-1,i+1), Quaternion.identity);
			Instantiate(templates.noRoom, new Vector2(yDimension,i), Quaternion.identity);

		}

		for(int i = 0; i<yDimension; i++){
			Instantiate(templates.noRoom, new Vector2(i,-1), Quaternion.identity);
		}
	}

	void SpawnDamselCell(){
		int cellLocation  = Random.Range(2*spawnableRoom.Count/4, spawnableRoom.Count);
		Debug.Log(cellLocation);
		GameObject roomWhichContainsCell = spawnableRoom[cellLocation];
		
		Instantiate(templates.damselRoom, roomWhichContainsCell.transform.position, Quaternion.identity);
		FurnitureSpawner furniture = roomWhichContainsCell.GetComponentInChildren<FurnitureSpawner>();
		furniture.DeleteFurniture();
		Destroy(roomWhichContainsCell);
	}

	// void Spawn

	//test
	// void Spawn(){

	// 	if(spawned == false){
	// 		if(openingDirection == 1){
	// 			rand = Random.Range(0, templates.bottomRooms.Length);
	// 			Instantiate(templates.bottomRooms[rand], transform.position, templates.bottomRooms[rand].transform.rotation);
	// 		} else if(openingDirection == 2){
	// 			rand = Random.Range(0, templates.topRooms.Length);
	// 			Instantiate(templates.topRooms[rand], transform.position, templates.topRooms[rand].transform.rotation);
	// 		} else if(openingDirection == 3 ){
	// 			rand = Random.Range(0, templates.leftRooms.Length);
	// 			Instantiate(templates.leftRooms[rand], transform.position, templates.leftRooms[rand].transform.rotation);
	// 		} else if(openingDirection == 4){
	// 			rand = Random.Range(0, templates.rightRooms.Length);
	// 			Instantiate(templates.rightRooms[rand], transform.position, templates.rightRooms[rand].transform.rotation);
	// 		}
	// 		spawned = true;
	// 		// Debug.Log(openingDirection);
	// 	}
		
	// }


	// void OnTriggerEnter2D(Collider2D other){
	// 	if(other.CompareTag("SpawnPoint") && other.GetComponent<RoomSpawner>().spawned == true){
	// 		Debug.Log("Des");
	// 		Destroy(gameObject);
	// 	}
	// }
}

