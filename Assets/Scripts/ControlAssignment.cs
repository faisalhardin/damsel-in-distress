﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlAssignment :  MonoBehaviour{

	public int player;
	public string attack;
	public string jump;
	public string dash;
	public string interact;
	public string useItem;
	public string swithLeft;
	public string switchRight;


	void Awake(){
		attack = "P" + player + "Attack";
		jump = "P" + player + "Jump";
		dash = "P" + player + "Dash";
		interact = "P" + player + "Interact";
		useItem = "P" + player + "UseItem";
		swithLeft = "P" + player + "SwitchLeft";
		switchRight = "P" + player + "SwitchRight";
	}

	
}
