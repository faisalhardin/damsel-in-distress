﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomPattern{
	public int width;
	public int height;
	public int[,] maze;

	public RoomPattern(int width, int height){
		this.width = width;
		this.maze = new int[height, width];
	}
}
