﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomPatternGenerator{
	public int mazeDimension;
	public int roomSize;
	public int[,] maze;
	public int THREE_SIZE = 3;
	public int ELEVATOR = 9;
	public int EMPTY = 0;

	public RoomPatternGenerator(int mazeDimension, int roomSize){
		this.mazeDimension = mazeDimension;
		this.maze = new int[mazeDimension, mazeDimension];
	}

}
