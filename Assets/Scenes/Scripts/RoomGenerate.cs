﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomGenerate{

	public RoomPattern maze;
	Queue<int[]> stairs = new Queue<int[]>();
	public int MAX_DEPTH = 6;
	public int BASE_MODIFIER = 4;

	public RoomGenerate(int width, int height){
		maze = new RoomPattern(width,height);
		generate(height-1);
	}

	public int[,] getRoomPattern(){
		return maze.maze;
	}

	public void generate(int height){
		
		int[] stair;
		traverse3(0, 0, 0, false, Random.Range(3,6));
		int maxIteration = 200;
		while(stairs.Count>0 && maxIteration-- > 0){
			stair = stairs.Dequeue();
			if(stair[0] < height){
				traverse3(stair[0], stair[1], 0, true,Random.Range(2,4));
			}else{
				maze.maze[stair[0],stair[1]] = 0;
			}
		}
	}

	public void traverse3(int x, int y, int depth, bool isGoingVertical,int modifier){
		if((x >= maze.maze.GetLength(0) || x < 0 || y >= maze.maze.GetLength(1) ||  y < 0 )){
			return;
		}
		if(depth >= MAX_DEPTH || (maze.maze[x,y] != 0 && maze.maze[x,y] != 8)) {
			return;
		}

		if(isGoingVertical){
			maze.maze[x,y] = 9;
			if(Random.Range(1,3) >1){
				traverse3(x,y+1,depth+1,false,Random.Range(2,5));
				traverse3(x,y-1,depth+1,false,Random.Range(2,5));
				
			} else {
				traverse3(x,y-1,depth+1,false,Random.Range(2,5));
				traverse3(x,y+1,depth+1,false,Random.Range(2,5));
			}
			stairs.Enqueue(new int[]{x+1,y});
		} else if(modifier>1 && maze.maze[x,y] == 0 ){
			if(Random.Range(1,4)>1){
				if(fillRoom(x,y,3,true)){
					traverse3(x,y+3,depth+1,false,modifier-1);
				}
				if( fillRoom(x,y,3,false)){
					traverse3(x,y-3,depth+1,false,modifier-1);
				}		
			} else {
				if(fillRoom(x,y,3,false)){
					traverse3(x,y-3,depth+1,false,modifier-1);
				}
				if(fillRoom(x,y,3,true)){
					traverse3(x,y+3,depth+1,false,modifier-1);
				}
			}
		} else if(maze.maze[x,y] == 0 || maze.maze[x,y] == 8){
			maze.maze[x,y] = 9;
			if(Random.Range(1,3) >1){
				traverse3(x,y+1,depth+1,false,Random.Range(2,5));
				traverse3(x,y-1,depth+1,false,Random.Range(2,5));
			} else {
				traverse3(x,y-1,depth+1,false,Random.Range(2,5));
				traverse3(x,y+1,depth+1,false,Random.Range(2,5));
			}
			maze.maze[x+1,y] = 8;
			stairs.Enqueue(new int[]{x+1,y});
		}

		return;

	}

	

	public bool fillRoom(int x, int y, int height, bool isToRight){
		if(isToRight){
			if( y+height < maze.maze.GetLength(1)){
					
					int i = height - 1;
					while(i>0 && maze.maze[x,y+i] == 0){
						i--;
					}
					if(i>0) return false;
					for(i = height-1; i >= 0; i--){
						maze.maze[x,y+i] = height;
						
					}
					
				} else return false;

		} else{
			if( y-height >= 0){
					int i = height - 1;
					while(i>=0 && maze.maze[x,y-i] == 0){
						i--;
					}
					if(i>0) return false;
					for(i = height -1; i >= 0 ; i--){
						maze.maze[x,y-i] = height;
					}
					
				} else return false;
		}
		
		return true;
	}
	
}